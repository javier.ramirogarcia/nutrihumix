
# Create DESeq and vsd objects for Caco2

# set path to the folder containing the annotation and count matrix objects.

setwd("~/PATH/TO/THE/annotation_and_count_matrix/OBJECT")

if (!require("DESeq2")) {
  install.packages("DESeq2")
  library(DESeq2)
}

if (!require("tidyverse")) {
  install.packages("tidyverse")
  library(tidyverse)
}


# Read LGG annotation and LGG count matrix.

gene_count_data_matrix = readRDS("caco2_count_matrix.RDS")
annotation = readRDS("caco2_annotation.RDS")

# Generate DESeq object using ~type * media as design.

dds <- DESeqDataSetFromMatrix(countData = gene_count_data_matrix,colData = annotation,design = ~ type * media)
dds <- DESeq(dds)

# Generate vsd object

vsd <- varianceStabilizingTransformation(dds, blind=FALSE)
saveRDS(vsd,"vsd_caco2.RDS")


# Calculate DE genes for the different comparisons.

res_type_cLGG_vs_C <- results(dds, 
                              alpha = 0.05, 
                              name = "type_CLGG_vs_C")

res_media_diet_HF_vs_diet__REF <- results(dds, 
                                          alpha = 0.05, 
                                          name = "media_diet_HF_vs_diet__REF")

res_typeCLGG.mediadiet_HF <- results(dds, 
                                     alpha=0.05, 
                                     name = "typeCLGG.mediadiet_HF")

