#!/bin/bash

# Usage message

usage="
\n---USAGE---\n
\n
    Description:\n\n
This script takes sam files as raw data and counts the number of reads aligned to each protein \n
using the start and stop position of both the alignment and the protein. Just one nucleotide \n
overlap is needed to report hit. If a read align to more than one protein all proteins are reported.\n
\n
    Required inputs\n
\n
-p Table containing protein start and stop positions [Protein table from NCBI]\n
-s Path to folder containing the sam files\n
\n\n
Example of usage:\n
$0 -p lgg_bc_protein_list -s sam_files \n\n
output:\n
sorted_alignment_files\n
protein_count_files\n\n
--- End of USAGE ---\n
"

# Help option, check number of argument.

if [[ "$1" == "-h" ]]
  then
    echo -e $usage
    exit 0
elif [[ $# -ne 4 ]]
  then
    echo -e "ERROR: Invalid number of arguments \nTry $0 -h for help"
    exit 1
fi


# Assign variables through flags

while getopts p:s: opt; do
    case "$opt" in
    p)
        protein_table_file=${OPTARG}
        ;;
    s)
        sam_containing_folder=${OPTARG}
        ;;
    esac
done

# Create needed folders.

mkdir -p sorted_alignment_files protein_count_files

# Create sorted alignment files

for file in $sam_containing_folder/*.sam; \
  do \
    fbname=$(basename "$file" .sam); \
    awk -F "\t" 'BEGIN{ \
      OFS="\t" \
    } \
    { \
      if($2==163){ \
        if($9<0 && $9>-500){ \
          print $3,$4,$4-$9-1,"+" \
        } \
        else{ \
          if($9<500){ \
            print $3,$4,$4+$9-1,"+" \
          } \
        } \
      }; \
      if($2==99){ \
        if($9<0 && $9>-500){ \
          print $3,$4,$4-$9-1,"-" \
        } \
        else{ \
          if($9<500){ \
            print $3,$4,$4+$9-1,"-" \
          } \
        } \
      } \
    }' $file | \
    sort -k1,1 -k2,2n -k3,3n | \
    uniq -c | \
    awk 'BEGIN{ \
      OFS="\t" \
    } \
    { \
      print $1,$2,$3,$4,$5 \
    }' > "sorted_alignment_files/"$fbname".sorted_alignment"
  done

# Create protein count files

for file in sorted_alignment_files/*.sorted_alignment; \
  do \
    fbname=$(basename "$file" .sorted_alignment); \
    awk -F "\t" 'BEGIN{ \
      OFS="\t" \
    } \
    { \
      if(FNR==1){ \
        x++ \
      } \
    } \
    { \
      if(x==1){ \
        if(substr($1,1,1)!="#"){ \
          arr_genome[$8]=$2; \
          arr_start[$8]=$3; \
          arr_end[$8]=$4; \
          arr_function[$8]=$11; \
          arr_strand[$8]=$5 \
        } \
      } \
    } \
    { \
      if(x==2){ \
        for(record in arr_genome){ \
          if(arr_genome[record]==$2 && arr_end[record]>=$3){ \
            if(arr_start[record]<=$4){ \
              if(arr_strand[record]==$5){ \
                print $0,record,arr_genome[record],arr_start[record],arr_end[record],arr_function[record],"sense" \
              } \
              else{ \
                print $0,record,arr_genome[record],arr_start[record],arr_end[record],arr_function[record],"antisense" \
              }
            } \
          } \
        } \
      } \
    }' $protein_table_file $file > "protein_count_files/"$fbname".protein_count"
  done

rm -r sorted_alignment_files
